# Exit on errors
set -o errexit

# Add commit hash to the README
export OWNER_NAME=$CI_PROJECT_NAMESPACE
export REPO_NAME=$CI_PROJECT_NAME
envsubst < webpage/README.md > webpage/README-complete.md
mv webpage/README-complete.md webpage/README.md

# Fetch and create gh-pages and output branches
# Travis does a shallow and single branch git clone
git remote set-branches --add origin gh-pages output
git fetch origin gh-pages:gh-pages output:output

ls -la
ls -la gh-pages
ls -la output
ls -la webpage

# Configure versioned webpage
python build/webpage.py \
  --checkout=gh-pages \
  --version=$CI_COMMIT_REF

# Generate OpenTimestamps
ots stamp \
  webpage/v/$CI_COMMIT_REF/index.html \
  webpage/v/$CI_COMMIT_REF/manuscript.pdf

# Commit message
MESSAGE="\
`git log --max-count=1 --format='%s'`

This build is based on
https://github.com/$CI_PROJECT_PATH/commit/$CI_COMMIT_REF.

This commit was created by the following Travis CI job:
https://travis-ci.org/$CI_PROJECT_PATH/jobs/$CI_JOB_ID

[ci skip]

The full commit message that triggered this build is copied below:

$TRAVIS_COMMIT_MESSAGE
"

# git \
#   --push \
#   --branch=output \
#   --message="$MESSAGE" \
#   output

# # Deploy the webpage directory to gh-pages
# git \
#   --follow-links \
#   --push \
#   --branch=gh-pages \
#   --message="$MESSAGE" \
#   webpage